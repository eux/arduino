// ---------------------------------------------------------------------------
// robot Arduino User Group Roma
// prova motori 
// ---------------------------------------------------------------------------

#include <Servo.h>       //include la libreria per la gestione del servo
#include <NewPing.h>     //include la libreria per la gestione del sensore a ultrasuoni

#define TRIGGER_PIN  4  // Pin di Arduino collegato al pin Trigger del sensore ad Ultrasuoni
#define ECHO_PIN     2  // Pin di Arduino collegato al pin Echo del sensore ad Ultrasuoni
#define MAX_DISTANCE 200 // Massima distanza che sarà gestita dal ping. sopra questo valore i ping vengono ignorati
#define SERVOPIN     8  // Pin di arduino cui è collegato il servo

// definiamo il sensore ad ultrasuoni e il servo
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // fai il setup del sensore
Servo uservo;                                        // definisci l'oggetto servo
int distMax=0;           // angolo di massima distanza                       
int distanza=0;          // valore della distanza in cm

// definiamo i pin del motore
// Motore sinistro  
int motoreSin1=5;  // Pin 1A del SN754410
int motoreSin2=6;  // Pin 2A del SN754410

// Motore destro
int motoreDes1=3;  // Pin 3A del SN754410
int motoreDes2=11; // Pin 4A del SN754410

// impostiamo la velocità - 0 (min) --> 255 (max)
// consigliato valore >= 150
int speed=250;


// ******************************************************************
// funzione invocata alla partenza di Arduino 1 volta soltanto
// serve per impostare le variabili iniziali
void setup() {
  Serial.begin(115200);     // Abilitiamo il serial monitor per la misurazione della distanza
  
  // impostazione iniziale del sensore ad ultrasuoni e del servo
  uservo.attach(SERVOPIN); // collega il servo al suo pin di comando 
  uservo.write(90);        // metti il servo dritto a 90 gradi
  delay(200);              // aspettiamo 300 millisecondi che il servo si sposti
  
  // set dei pin di Arduino collegati al motore come OUTPUT
  pinMode(motoreSin1, OUTPUT);
  pinMode(motoreSin2, OUTPUT);
  pinMode(motoreDes1, OUTPUT);
  pinMode(motoreDes2, OUTPUT);
  stop(); //ferma i motori

}


// ******************************************************************
// Funzione richiamata ciciclamente finchè l'Arduino è in funzione
void loop() {
  // ferma i motori
 
  // Ritardo in millisecondi - 1000 = 1 seconds.
  
  // comincia ad avanzare  
  avanti(speed); //vai avanti con velocità speed
  
  distanza=leggiDistanza();            // leggi la distanza del sensore ad ultrasioni
  
  if (distanza > 0 && distanza < 25)   // se vero hai trovato un ostacolo
  {
    stop();    //ferma i motori 

    int angoloFuga= scandisciAmbiente();     //calcola il miglior angolo di fuga
    indietro(speed);
    delay(500);
    stop();

    int angoloRotazione=angoloFuga-90;       // calcola la rotazione da dare rispetto ai 90 gradi

    if (angoloRotazione > 0)
    {
      //se >0 devo girare a sinistra
      sinistra(speed);            //gira a sinistra
      delay(300);   //calcola il tempo per girare a sinistra
    } else {
      //se <0 devo girare a destra
      destra(speed);                  //gira a destra
      delay(300);  //calcola il tempo per girare a destra
    }
  }
  
}


