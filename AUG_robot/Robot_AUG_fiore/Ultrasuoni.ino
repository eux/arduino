// ******************************************************************************************
// leggi la distanza con il servo ad ultrasuoni in cm
int leggiDistanza(){
  unsigned int uS = sonar.ping_cm();        //  manda un ping e conta il tempo di risposta (uS).
  int distanzaCM = uS; // converti il tempo in distanza (0 = fuori dal range di distanza, nessun echo del ping)
  return(distanzaCM);                    // restituisci la distanza al chiamante
} 


// ******************************************************************************************
// controlla gli ostacoli ogni 30 gradi 
int scandisciAmbiente()
{
  int ang=0;        //indice angolo usato per la scansione
  int angoloMax=0;  //angolo con la distanza max di uscita
  distMax=0;   // imposta la distanza di uscita migliore a zero 
  for (ang=4; ang<=8; ang++) // fai un ciclo per controllare dove sono gli ostacoli 
  {
    int angolo=ang*15; // scandisci ogni 30 gradi
    uservo.write(angolo); // imposta il servo degli ultrasuoni sul valore assunto da ang 
    delay(250); //attendi allineamento servo ultrasuoni 
    int distanzaAngolo=leggiDistanza(); //leggi la distanza per questo angolo
    Serial.print("Angolo: ");
    Serial.print(angolo);
    Serial.print(" Gradi"); 
    Serial.print("  Ping: "); 
    Serial.print(distanzaAngolo); 
    Serial.println("cm");
    if (distanzaAngolo > distMax) // verifica che la distanza letta sia maggiore del max 
    {
      angoloMax=angolo;  // se e' maggiore del max imposta il nuovo angolo max e
      distMax=distanzaAngolo;  // la nuova distanza max 
    }
  } //fine ciclo for
  uservo.write(90); //rimetti il servo degli ultrasuoni dritto
   
  return(angoloMax); 

}

