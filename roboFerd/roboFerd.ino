#include <Servo.h>

int go(int l,int r ,int dur );

Servo servoL;
Servo servoR;
Servo servoHead;

int servoLZero = 85; // experimentally found to stop  L motor
int servoRZero = 82; // experimentally found to stop  R motor

int onboardLed = 13;
int power = 10;



int calibrate = 0;

//sonar pins
const int pingPin = 7;
const int echoPin = 8;


void setup() {
  pinMode(onboardLed, OUTPUT);
  digitalWrite(onboardLed, HIGH);
  Serial.begin(9600); 
  servoL.attach(9);
  servoR.attach(10);
  servoHead.attach(11);
  go(0,0,500);
  head(90);
  
   pinMode(pingPin, OUTPUT);
   pinMode(echoPin, INPUT);
   delay(5000);
   
}

void loop() {
  int left;
  int right;
  if(ping() < 20)
  {
     go(0,0,1000);
     head(0);
     left = ping();
     head(180);
     right = ping();
     head(90);
     if(right > left)
     {
        go(-20,20,500);
     }
     else
     {
       go(20,-20,500);
     }
    Serial.println("Aarg turn");
    //go(40,-40,100);
     go(0,0,100);
    delay(1000);
    
  }
  else
  {
    Serial.println("Poptiedom");
    go(40,40,500);
  }
  //Serial.println(ping());
  //Serial.println(0);
  //head.write(0);
  //delay(2000);
  //Serial.println(85);
  //head.write(90);
  //delay(2000);
  //Serial.println(180);
  //head.write(180);
  //delay(2000);
  
}

int go(int l,int r ,int dur = 500)
{
    servoL.write( servoLZero +l );
    servoR.write(servoRZero -  r); 
    delay(dur);
    return 0;
}

int head(int pos)
{
   servoHead.write(pos);
   delay(1000);
}


long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

long ping()
{
  long duration, inches, cm;
// The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);

  // The same pin not is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  //are reading the echo pin here
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);

  // convert the time into a distance
  //inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);
   delay(100);
 return cm;
  
 
}



