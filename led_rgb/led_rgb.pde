/**
led schema - http://www.instructables.com/id/RGB-LED-Tutorial-using-an-Arduino-RGBL/step2/Testing/
*/

int ledAnalogOne[] = {3, 5, 6}; //the three pins of the first analog LED 3 = redPin, 5 = greenPin, 6 = bluePin
int MAX_VALUE = 255;
int incremental;
int currentLed;

void setup() {
  for(int i = 0; i < 3; i++){
    pinMode(ledAnalogOne[i], OUTPUT);
    //Set the three LED pins as outputs
  }
  
  incremental = 0;
  currentLed = 0;
  
  analogWrite(ledAnalogOne[0], 0);
  analogWrite(ledAnalogOne[1], 0);
  analogWrite(ledAnalogOne[2], 0);
}

void loop(){
  
  for (int led = 0; led <= 2; led++) {
    for (int i = 0; i <= MAX_VALUE; i++) {
      analogWrite(ledAnalogOne[led], i);
      delay(200);
    }
  }
    
  analogWrite(ledAnalogOne[0], 0);
  analogWrite(ledAnalogOne[1], 0);
  analogWrite(ledAnalogOne[2], 0); 
}

