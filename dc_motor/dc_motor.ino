

//usa il transistor bc547
int ledPin = 6;    // LED connected to digital pin 9

void setup()  { 
  // nothing happens in setup 
} 

void loop()  { 
  //botta per farlo partire
  analogWrite(ledPin, 255); 
  delay(2500);
  
  // fade in from min to max in increments of 5 points:
  for(int fadeValue = 150 ; fadeValue <= 255; fadeValue +=5) { 
    // sets the value (range from 0 to 255):
    analogWrite(ledPin, fadeValue);         
    // wait for 30 milliseconds to see the dimming effect    
    delay(30);                            
  } 

  // fade out from max to min in increments of 5 points:
  for(int fadeValue = 255 ; fadeValue >= 150; fadeValue -=5) { 
    // sets the value (range from 0 to 255):
    analogWrite(ledPin, fadeValue);         
    // wait for 30 milliseconds to see the dimming effect    
    delay(30);                            
  } 
  analogWrite(ledPin, 0); 
  delay(2000);
}


