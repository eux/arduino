#include <NewPing.h>

#define TRIGGER_PIN  3  // Pin di Arduino collegato al pin Trigger del sensore ad Ultrasuoni
#define ECHO_PIN     2  // Pin di Arduino collegato al pin Echo del sensore ad Ultrasuoni
#define MAX_DISTANCE 100 // Massima distanza che sarà gestita dal ping. sopra questo valore i ping vengono ignorati

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // fai il setup del sensore

void setup() {
  Serial.begin(115200);     // Abilitiamo il serial monitor per la misurazione della distanza
}

void loop() {

  int distanza = leggiDistanza();            // leggi la distanza del sensore ad ultrasioni
  
  Serial.print("Distanza: ");
  Serial.println(distanza);
  
  delay(300);
}

int leggiDistanza(){
  unsigned int uS = sonar.ping();        //  manda un ping e conta il tempo di risposta (uS).
  int distanzaCM = uS / US_ROUNDTRIP_CM; // converti il tempo in distanza (0 = fuori dal range di distanza, nessun echo del ping)
  return(distanzaCM);                    // restituisci la distanza al chiamante
} 


