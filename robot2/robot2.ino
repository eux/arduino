/* 
 Plays random notes from a pentatonic sequence with potentiometer control of note heights and tempo
 
 for a fritzing circuit representation see fritzing folder
 
 created 19 Jan 2013
 by Eugenio Pombi
 @euxpom
 
 This example code is in the public domain.
 */
#include "pitches.h"
#include "pentatonic.h"
#include "leds.h"

int speakerOut = 13; 
int potentiometerPin1 = 1;  //Analog in 1 - speed 
int potentiometerPin2 = 2;  //Analog in 2 - height of melody's notes 

int melodySpeed = 0;
int melodyHeight = 0;  
int randomNumber = 0;  
  
void setup() {
  //Serial.begin(9600);
  for (int i = 0; i < 5; i++) {
    pinMode(leds[i], OUTPUT);
  }
  
  int melodySpeed = 0;
  int melodyHeight = 0;
  int randomNumber = 0;
}

void loop() 
{    
  melodySpeed = analogRead(potentiometerPin1); 
  melodySpeed = map(melodySpeed, 0, 1024, 20, 500);
  
  melodyHeight = analogRead(potentiometerPin2); 
  melodyHeight = map(melodyHeight, 0, 1024, 0, 4);
  
  randomNumber = random(0, 5);
  
  //Serial.println(randomNumber);
  //Serial.println(leds[randomNumber]);
  //Serial.println("----");
  
  /*
  Serial.print("height: ");
  Serial.print(melodyHeight);
  Serial.print("; ");
  Serial.print("speed: ");
  Serial.print(melodySpeed);
  Serial.print("\n");
  Serial.print("random: ");
  Serial.print(randomNumber);
  Serial.print("\n");
  */
  
  digitalWrite(leds[randomNumber], HIGH);
  tone(speakerOut, pentatonic[melodyHeight][randomNumber], 1000);
  delay(melodySpeed);
  digitalWrite(leds[randomNumber], LOW);
  
  // stop the tone playing:
  noTone(speakerOut);  
}


