//inspired by http://arduino.cc/en/Tutorial/ShiftOut

#include "characters.h"

int latchPinGrounds = 8;
int clockPinGrounds = 12;
int dataPinGrounds = 11;

int statusRows[8] = {
  B00000001,
  B10000000,
  B01000000,
  B00100000,
  B00010000,
  B00001000,
  B00000100,
  B00000010,
};

#define SEQUENCE_SIZE 5
int* sequence[SEQUENCE_SIZE];

void setup() 
{                
  pinMode(latchPinGrounds, OUTPUT);
  pinMode(clockPinGrounds, OUTPUT);
  pinMode(dataPinGrounds, OUTPUT);
  
  sequence[0] = statusGroundsHeart;
  sequence[1] = statusGroundsL;
  sequence[2] = statusGroundsO;
  sequence[3] = statusGroundsV;
  sequence[4] = statusGroundsE;
}

void loop() 
{ 
  int letterPersistence = 60;  
  int intermediateDelay = 100;
  
  for (int currentLetter = 0; currentLetter < SEQUENCE_SIZE; currentLetter++) {
    for (int times = 0; times < letterPersistence; times++) { 
      for (int i = 0; i < 8; i++) {
        shiftRegisterWrite(B00000000, B00000000);
        shiftRegisterWrite(statusRows[i], sequence[currentLetter][i]);
        
        delay(2);      
      }
    }
  }  
}

void shiftRegisterWrite(int valueRow, int valueGround) 
{
  //prepare it to receive data
  digitalWrite(latchPinGrounds, LOW);
  //write data  
  shiftOut(dataPinGrounds, clockPinGrounds, MSBFIRST, valueRow);  //this will cascade
  shiftOut(dataPinGrounds, clockPinGrounds, MSBFIRST, valueGround);
  
  //send data to pins
  digitalWrite(latchPinGrounds, HIGH);
}
