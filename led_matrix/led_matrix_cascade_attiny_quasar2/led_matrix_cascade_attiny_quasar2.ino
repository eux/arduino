//inspired by http://arduino.cc/en/Tutorial/ShiftOut

#include "characters.h"

int latchPinGrounds = 2;   //arduino 8   - attiny 2
int clockPinGrounds = 0;   //arduino 12  - attiny 0
int dataPinGrounds =  1;   //arduino 11  - attiny 1

int statusRows[8] = {
  B00000001,
  B10000000,
  B01000000,
  B00100000,
  B00010000,
  B00001000,
  B00000100,
  B00000010,
};

#define SEQUENCE_SIZE 22
int* sequence[SEQUENCE_SIZE];

void setup() 
{                
  pinMode(latchPinGrounds, OUTPUT);
  pinMode(clockPinGrounds, OUTPUT);
  pinMode(dataPinGrounds, OUTPUT);
  
  sequence[0] = statusGroundsLogo;
  sequence[1] = statusGroundsOff;
  sequence[2] = statusGroundsLogo;
  sequence[3] = statusGroundsOff;
  sequence[4] = statusGroundsW;
  sequence[5] = statusGroundsE;
  sequence[6] = statusGroundsOff;
  sequence[7] = statusGroundsA;
  sequence[8] = statusGroundsR;
  sequence[9] = statusGroundsE;
  sequence[10] = statusGroundsOff;
  sequence[11] = statusGroundsT;
  sequence[12] = statusGroundsH;
  sequence[13] = statusGroundsE;
  sequence[14] = statusGroundsOff;
  sequence[15] = statusGroundsR;
  sequence[16] = statusGroundsO;
  sequence[17] = statusGroundsB;
  sequence[18] = statusGroundsO;
  sequence[19] = statusGroundsT;
  sequence[20] = statusGroundsS;
  sequence[21] = statusGroundsOff;
}

void loop() 
{ 
  int letterPersistence = 50;  
  
  for (int currentLetter = 0; currentLetter < SEQUENCE_SIZE; currentLetter++) {
    for (int times = 0; times < letterPersistence; times++) { 
      for (int i = 0; i < 8; i++) {
        shiftRegisterWrite(B00000000, B00000000);
        shiftRegisterWrite(statusRows[i], sequence[currentLetter][i]);
        
        //delay(2);      
      }
    }
  }  
}

void shiftRegisterWrite(int valueRow, int valueGround) 
{
  //prepare it to receive data
  digitalWrite(latchPinGrounds, LOW);
  //write data  
  shiftOut(dataPinGrounds, clockPinGrounds, MSBFIRST, valueRow);  //this will cascade
  shiftOut(dataPinGrounds, clockPinGrounds, MSBFIRST, valueGround);
  
  //send data to pins
  digitalWrite(latchPinGrounds, HIGH);
}
