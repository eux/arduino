//inspired by http://arduino.cc/en/Tutorial/ShiftOut
int latchPinGrounds = 8;
int clockPinGrounds = 12;
int dataPinGrounds = 11;

int latchPin2 = 2;
int clockPin2 = 7;
int dataPin2 = 6;

int statusGroundsHeart[8] = {
  B10011001,
  B00000000,
  B00000000,
  B00000000,
  B10000001,
  B11000011,
  B11100111,
  B11110111,
};

int statusGroundsL[8] = {
  B00011111,
  B00011111,
  B00011111,
  B00011111,
  B00011111,
  B00000000,
  B00000000,
  B00000000,
};

int statusGroundsO[8] = {
  B00000000,
  B00000000,
  B00011000,
  B00011000,
  B00011000,
  B00011000,
  B00000000,
  B00000000,
};

int statusGroundsV[8] = {
  B00011000,
  B00011000,
  B00011000,
  B00011000,
  B00011000,
  B00000000,
  B11000011,
  B11100111,
};

int statusGroundsE[8] = {
  B00000000,
  B00000000,
  B00011111,
  B00000000,
  B00000000,
  B00011111,
  B00000000,
  B00000000,
};

int statusRows[8] = {
  B00000001,
  B10000000,
  B01000000,
  B00100000,
  B00010000,
  B00001000,
  B00000100,
  B00000010,
};

void setup() 
{                
  pinMode(latchPinGrounds, OUTPUT);
  pinMode(clockPinGrounds, OUTPUT);
  pinMode(dataPinGrounds, OUTPUT);
  
  pinMode(latchPin2, OUTPUT);
  pinMode(clockPin2, OUTPUT);
  pinMode(dataPin2, OUTPUT);
  
  Serial.begin(9600);
}


void loop() 
{ 
  int letterPersistence = 60;  
  int intermediateDelay = 100;
  
  for (int times = 0; times < letterPersistence; times++) { 
    for (int i = 0; i < 8; i++) {
      shiftRegisterGroundsWrite(B11111111);
      shiftRegisterRowsWrite(statusRows[i]);
      shiftRegisterGroundsWrite(statusGroundsHeart[i]);
      
      delay(2);      
    }
  }
  
  //clears last line
  shiftRegisterGroundsWrite(B11111111);
  shiftRegisterRowsWrite(statusRows[7]);
  delay(intermediateDelay);
  
  for (int times = 0; times < letterPersistence; times++) {
    for (int i = 0; i < 8; i++) {
      shiftRegisterGroundsWrite(B11111111);
      shiftRegisterRowsWrite(statusRows[i]);
      shiftRegisterGroundsWrite(statusGroundsL[i]);
      delay(2);
    }
  }
  
  //clears last line
  shiftRegisterGroundsWrite(B11111111);
  shiftRegisterRowsWrite(statusRows[7]);
  delay(intermediateDelay);
  
  for (int times = 0; times < letterPersistence; times++) {
    for (int i = 0; i < 8; i++) {
      shiftRegisterGroundsWrite(B11111111);
      shiftRegisterRowsWrite(statusRows[i]);
      shiftRegisterGroundsWrite(statusGroundsO[i]);
      delay(2);
    }
  }
  
  //clears last line
  shiftRegisterGroundsWrite(B11111111);
  shiftRegisterRowsWrite(statusRows[7]);
  delay(intermediateDelay);
  
  for (int times = 0; times < letterPersistence; times++) {
    for (int i = 0; i < 8; i++) {
      shiftRegisterGroundsWrite(B11111111);
      shiftRegisterRowsWrite(statusRows[i]);
      shiftRegisterGroundsWrite(statusGroundsV[i]);
      delay(2);
    }
  }
  
  //clears last line
  shiftRegisterGroundsWrite(B11111111);
  shiftRegisterRowsWrite(statusRows[7]);
  delay(intermediateDelay);
  
  for (int times = 0; times < letterPersistence; times++) {
    for (int i = 0; i < 8; i++) {
      shiftRegisterGroundsWrite(B11111111);
      shiftRegisterRowsWrite(statusRows[i]);
      shiftRegisterGroundsWrite(statusGroundsE[i]);
      delay(2);
    }
  }
  
  //clears last line
  shiftRegisterGroundsWrite(B11111111);
  shiftRegisterRowsWrite(statusRows[7]);
  delay(intermediateDelay);
}

void shiftRegisterGroundsWrite(int value) 
{
  //prepare it to receive data
  digitalWrite(latchPinGrounds, LOW);
  //write data  
  shiftOut(dataPinGrounds, clockPinGrounds, MSBFIRST, value);
  //send data to pins
  digitalWrite(latchPinGrounds, HIGH);
}

void shiftRegisterRowsWrite(int value) 
{
  //prepare it to receive data
  digitalWrite(latchPin2, LOW);
  //write data  
  shiftOut(dataPin2, clockPin2, MSBFIRST, value);
  //send data to pins
  digitalWrite(latchPin2, HIGH);
}
