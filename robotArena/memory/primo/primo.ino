//http://www.hobbytronics.co.uk/arduino-external-eeprom

#include <Wire.h>    
 
#define disk1 0x50    //Address of 24LC256 eeprom chip

int potPin = 3; 
 
void setup(void)
{
  Serial.begin(9600);
  Wire.begin();  
  
  Serial.println("BEGIN!");
  
  unsigned int addressStep1 = 0;
  unsigned int addressStep2 = 1;
  unsigned int addressStep3 = 2;
 
  int potVal = analogRead(potPin);
  
  writeEEPROM(disk1, addressStep1, 1);
  int valueStep1 = readEEPROM(disk1, addressStep1);
  Serial.println(valueStep1, DEC);
  
  if (valueStep1 == 1) {
      Serial.println("ho individuato l'uguaglianza");  
  }
  if (valueStep1 == 23) {
      Serial.println("se entro qui c'è un problema");
  }
  
  writeEEPROM(disk1, addressStep2, 120);
  Serial.println(readEEPROM(disk1, addressStep2), DEC);
  
  writeEEPROM(disk1, addressStep3, potVal);
  Serial.print("POTENZIOMETRO: ");
  Serial.println(readEEPROM(disk1, addressStep3), DEC);
  
}
 
void loop(){}
 
void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data ) 
{
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));   // MSB
  Wire.write((int)(eeaddress & 0xFF)); // LSB
  Wire.write(data);
  Wire.endTransmission();
 
  Serial.println("WRITTEN");
  delay(5);
}
 
byte readEEPROM(int deviceaddress, unsigned int eeaddress ) 
{
  byte rdata = 0xFF;
 
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));   // MSB
  Wire.write((int)(eeaddress & 0xFF)); // LSB
  Wire.endTransmission();
 
  Wire.requestFrom(deviceaddress,1);
 
  if (Wire.available()) rdata = Wire.read();
 
  return rdata;
}
