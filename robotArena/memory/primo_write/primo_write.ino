//http://www.hobbytronics.co.uk/arduino-external-eeprom

#include <Wire.h>    
 
#define disk1 0x50    //Address of 24LC256 eeprom chip
 
void setup(void)
{
  Serial.begin(9600);
  Wire.begin();  
  
  Serial.println("BEGIN!");
  
  unsigned int addressStep1 = 0;
  unsigned int addressStep2 = 1;
  unsigned int addressStep3 = 2;
 
  
  writeEEPROM(disk1, addressStep1, 1);
  writeEEPROM(disk1, addressStep2, 120);
  writeEEPROM(disk1, addressStep3, 254);
}
 
void loop(){}

void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data ) 
{
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));   // MSB
  Wire.write((int)(eeaddress & 0xFF)); // LSB
  Wire.write(data);
  Wire.endTransmission();

  delay(5);
}
