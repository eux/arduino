ROBOT

movimento
- due servo a rotazione continua
- dc motor con ponte H
- dc motor con driver pololu

encoder
- encoder circolari a luce bianca? 

ricezione istruzioni

- un lettore/ricevitore delle istruzioni (movimento/shooting) 
- esecutore delle istruzioni ricevute (movimento/shooting)
- feedback sullo stato di ricezione

shooting system out

- un cannoncino IR fisso sul petto
- sistema di feedback quando parte lo sparo (laser?)
- buzzer + trimmer

shooting system in

- ricevitori sui fianchi e sul retro (ognuno con il suo ATTiny?)
- sistema di feedback quando viene colpito (ogni lato può essere colpito una sola volta a tick)
- buzzer + trimmer (comune al buzzer dello shooting system out)

PLANCE

sistema di programmazione

- plancia con 4 slot di programmazione movimento 
- 4 potenziometri per la velocità e 
- 4 trigger/switch shooting
- vari componenti di programmazione movimento(avanti, dx, sx, indietro)
- sistema di feedback con led per indicare il corretto posizionamento dei componenti di programmazione
- sistema di feedback con led per indicare lo stato dello shooting
- sistema di feedback con seven segments per indicare la lettura dei potenziometri (0-9?)

sistema di trasferimento

- programmazione di chip di memoria
- invio tramite radio
- bottone avvio trasmissione/scrittura su memoria in base alle posizioni degli slot/potenziometri/switch in plancia
- feedback tramite led sullo stato di trasmissione/scrittura


CONTROLLORE (tramite radio o tramite IR)

- invia dei tick per far eseguire ai robot in sincrono i diversi step dei 4 slot di programmazione
- display del tick corrente
- bottone per iniziare un nuovo tick 
