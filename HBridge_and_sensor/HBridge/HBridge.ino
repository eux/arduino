const int switchPin = 2;     // switch input

const int motor1Pin1 = 3;    // H-bridge leg 1 (pin 2, 1A)
const int motor1Pin2 = 4;    // H-bridge leg 2 (pin 7, 2A)
const int motor1Enable = 9;  // H-bridge enable pin

const int motor2Pin1 = 5;    // H-bridge leg 1 (pin 2, 1A)
const int motor2Pin2 = 7;    // H-bridge leg 2 (pin 7, 2A)
const int motor2Enable = 10; // H-bridge enable pin  

void setup() {
  Serial.begin(9600); 
  
  // set the switch as an input:
  pinMode(switchPin, INPUT);

  // set all the other pins you're using as outputs:
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(motor1Enable, OUTPUT);
  
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);
  pinMode(motor2Enable, OUTPUT);
  
  // set enablePin high so that motor can turn on:
  digitalWrite(motor1Enable, HIGH);
  digitalWrite(motor2Enable, HIGH);
}

void loop() {
  
    
    digitalWrite(motor1Pin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motor1Pin2, HIGH);  // set leg 2 of the H-bridge high
    digitalWrite(motor2Pin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motor2Pin2, HIGH);  // set leg 2 of the H-bridge high
    delay(2000);  
    
    digitalWrite(motor1Pin1, LOW);  // set leg 1 of the H-bridge high
    digitalWrite(motor1Pin2, HIGH);   // set leg 2 of the H-bridge low
    digitalWrite(motor2Pin1, HIGH);  // set leg 1 of the H-bridge high
    digitalWrite(motor2Pin2, LOW);   // set leg 2 of the H-bridge low
    delay(1000);
    
}

