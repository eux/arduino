#include <DualHBridge.h>
#include <Servo.h>

Servo servoSensor;

DualHBridge motors;

const int IRpin = 1;  

const int servoSensorStartingPosition = 112;

void setup() {
  Serial.begin(9600); 
  
  motors.attachLeftEnablePin(9);
  motors.attachLeftPin1(3);
  motors.attachLeftPin2(4);
    
  motors.attachRightEnablePin(10);
  motors.attachRightPin1(5);
  motors.attachRightPin2(7);

  servoSensor.attach(11);
  servoSensor.write(servoSensorStartingPosition);
    
  motors.enableMotors();
}

void loop() {
  float distance = getDistanceAt(servoSensorStartingPosition);
  
  if (distance > 60) {
    motors.moveForward();
  } else {

    motors.halt();
    
    int distanceRight = getDistanceAt(servoSensorStartingPosition + 40);
    int distanceLeft = getDistanceAt(servoSensorStartingPosition - 40);
    
    if (distanceLeft > distanceRight) {
      motors.pivotLeft();   
    } else {
      motors.pivotRight();
    }
    delay(1500);
    motors.halt();
  }
}

float getDistanceAt(int servoPosition) 
{
  servoSensor.write(servoPosition);  
  delay(300);
  
  float volts = analogRead(IRpin)*0.0048828125;   // value from sensor * (5/1024) - if running 3.3.volts then change 5 to 3.3
  float distance = 65*pow(volts, -1.10);          // worked out from graph 65 = theretical distance / (1/Volts)S - luckylarry.co.uk
  
  return distance;
}

