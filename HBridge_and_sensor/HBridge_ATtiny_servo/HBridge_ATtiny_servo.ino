#include <SoftwareServo.h> 

SoftwareServo myservo;

const int motor1Pin1 = 3;    // H-bridge leg 1 (pin 2, 1A)
const int motor1Pin2 = 4;    // H-bridge leg 2 (pin 7, 2A)

const int motor2Pin1 = 1;    // atTiny 1 arduino 8
const int motor2Pin2 = 0;    // atTiny 0 arduino 9

void setup() {
  //Serial.begin(9600); 

  myservo.attach(2);
  myservo.write(90);

  // set all the other pins you're using as outputs:
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);
}

void loop() {
    SoftwareServo::refresh();
    myservo.write(15);
    SoftwareServo::refresh();    
    
    digitalWrite(motor1Pin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motor1Pin2, HIGH);  // set leg 2 of the H-bridge high
    digitalWrite(motor2Pin1, LOW);   // set leg 1 of the H-bridge low
    digitalWrite(motor2Pin2, HIGH);  // set leg 2 of the H-bridge high
    
    for (int i = 0; i < 10; i++) {
        delay(50);
       SoftwareServo::refresh();    
    }
    
    delay(1000);  
    SoftwareServo::refresh();
    myservo.write(170);
    SoftwareServo::refresh(); 
    for (int i = 0; i < 10; i++) {
        delay(50);
       SoftwareServo::refresh();    
    }
    
    
    digitalWrite(motor1Pin1, LOW);  // set leg 1 of the H-bridge high
    digitalWrite(motor1Pin2, LOW);   // set leg 2 of the H-bridge low
    digitalWrite(motor2Pin1, LOW);  // set leg 1 of the H-bridge high
    digitalWrite(motor2Pin2, LOW);   // set leg 2 of the H-bridge low
    delay(1700);
}

