#include <Servo.h>

Servo servoSensor;

const int motor1Pin1 = 3;    // H-bridge leg 1 (pin 2, 1A)
const int motor1Pin2 = 4;    // H-bridge leg 2 (pin 7, 2A)
const int motor1Enable = 9;  // H-bridge enable pin

const int motor2Pin1 = 5;    // H-bridge leg 1 (pin 2, 1A)
const int motor2Pin2 = 7;    // H-bridge leg 2 (pin 7, 2A)
const int motor2Enable = 10; // H-bridge enable pin
const int IRpin = 1;  

const int servoSensorStartingPosition = 112;

void setup() {
  Serial.begin(9600); 
  
  servoSensor.attach(11);
  servoSensor.write(servoSensorStartingPosition);
  
  // set all the other pins you're using as outputs:
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(motor1Enable, OUTPUT);
  
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);
  pinMode(motor2Enable, OUTPUT);
  
  // set enablePin high so that motor can turn on:
  digitalWrite(motor1Enable, HIGH);
  digitalWrite(motor2Enable, HIGH);
}

void loop() {
  float distance = getDistanceAt(servoSensorStartingPosition);
  
  if (distance > 60) {
    moveForward();
  } else {
    
    halt();
    
    int distanceRight = getDistanceAt(servoSensorStartingPosition + 40);
    int distanceLeft = getDistanceAt(servoSensorStartingPosition - 40);
    
    if (distanceLeft > distanceRight) {
      pivotLeft();   
    } else {
      pivotRight();
    }
    delay(1500);
    halt();
  }
}

float getDistanceAt(int servoPosition) 
{
  servoSensor.write(servoPosition);  
  delay(1000);
  
  float volts = analogRead(IRpin)*0.0048828125;   // value from sensor * (5/1024) - if running 3.3.volts then change 5 to 3.3
  float distance = 65*pow(volts, -1.10);          // worked out from graph 65 = theretical distance / (1/Volts)S - luckylarry.co.uk
  
  return distance;
}

void moveForward() 
{
  digitalWrite(motor1Pin1, LOW);   
  digitalWrite(motor1Pin2, HIGH);  
    
  digitalWrite(motor2Pin1, LOW);   
  digitalWrite(motor2Pin2, HIGH);
}

void moveBackward() 
{
  digitalWrite(motor1Pin1, HIGH);   
  digitalWrite(motor1Pin2, LOW);  
    
  digitalWrite(motor2Pin1, HIGH);   
  digitalWrite(motor2Pin2, LOW);
}

void halt()
{
  digitalWrite(motor1Pin1, LOW);   
  digitalWrite(motor1Pin2, LOW);  
    
  digitalWrite(motor2Pin1, LOW);   
  digitalWrite(motor2Pin2, LOW);
}

void pivotLeft()
{
  digitalWrite(motor1Pin1, HIGH);   
  digitalWrite(motor1Pin2, LOW);  
    
  digitalWrite(motor2Pin1, LOW);   
  digitalWrite(motor2Pin2, HIGH);
}

void pivotRight()
{
  digitalWrite(motor1Pin1, LOW);   
  digitalWrite(motor1Pin2, HIGH);  
    
  digitalWrite(motor2Pin1, HIGH);   
  digitalWrite(motor2Pin2, LOW);
}
