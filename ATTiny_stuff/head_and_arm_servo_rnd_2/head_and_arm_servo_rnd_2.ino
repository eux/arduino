#include <SoftwareServo.h> 

SoftwareServo servoArm;
SoftwareServo servoHead;

boolean servoArmGo;
boolean servoHeadGo;

void setup() {
  //Serial.begin(9600);
    
  servoHead.attach(0);
  servoHead.write(90);
  
  servoArm.attach(3);
  servoArm.write(90);

}

void loop() {
    int waitRand = random(800, 3000);
    
    //Serial.print("waitRand=");
    //Serial.println(waitRand);
    
    if ((waitRand % 3) == 0) { 
        moveAServo(servoHead, servoHeadGo);
        //Serial.println("moveHead");
    } else {
        moveAServo(servoArm, servoArmGo);
        //Serial.println("moveArm");
    }
    
    delay(waitRand);
}

void moveAServo (SoftwareServo& aServo, boolean& aServoStatus) {
    int degreeNumber;
    
    if (aServoStatus == false) {
        degreeNumber = 110;
        aServoStatus = true;
        //Serial.println("servo is now true before it was false");
    } else {
        degreeNumber = 70;
        aServoStatus = false;
        //Serial.println("servo is now false before it was true");
    }
    
    
    
    SoftwareServo::refresh();
    aServo.write(degreeNumber);
    SoftwareServo::refresh();    
    
    for (int i = 0; i < 30; i++) {
       delay(50);
       SoftwareServo::refresh();    
    }
}

