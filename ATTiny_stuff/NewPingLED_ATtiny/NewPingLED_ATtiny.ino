#include <NewPingATTiny.h>

#define TRIGGER_PIN  1  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     2  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

const int ledpin = 0;
NewPingATTiny sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.


void setup() {
  pinMode(ledpin, OUTPUT);
}

void loop()
{
  int dist = sonar.ping_median(5);   //median off 5 values
  dist = sonar.convert_cm(dist);     //convert that to cm, replace "cm" with "in" for inches 
  if (dist < 20 & dist != 0)                 // If an object is within 20cm,
  {
    digitalWrite(ledpin, HIGH);  // Turn on the LED
  }
  else
  {
    digitalWrite(ledpin, LOW);   //else, turn it off
  }
  delay(30);                     // Make sure there are no stray echoes bouncing around
}





