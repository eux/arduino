#include <SoftwareServo.h> 

SoftwareServo servoArm;
SoftwareServo servoHead;

boolean servoArmGo;
boolean servoHeadGo;

void setup() {
    
  servoHead.attach(3);
  servoHead.write(90);
  
  servoArm.attach(0);
  servoArm.write(90);

}

void loop() {
    int waitRand = random(800, 3000);
    
    if ((waitRand % 2) == 0) { 
        moveAServo(servoHead, servoHeadGo);
    } else {
        moveAServo(servoArm, servoArmGo);
    }
    
    SoftwareServo::refresh();
    servoHead.write(15);
    servoArm.write(15);
    SoftwareServo::refresh();    
    
    for (int i = 0; i < 30; i++) {
       delay(50);
       SoftwareServo::refresh();    
    }
    
    delay(1000);  
    SoftwareServo::refresh();
    servoArm.write(170);
    servoHead.write(170);
    SoftwareServo::refresh(); 
    
    for (int i = 0; i < 30; i++) {
        delay(50);
       SoftwareServo::refresh();    
    }
    
    int waitRand = random(800, 3000);
    
    delay(waitRand);
}

void moveAServo (SoftwareServo aServo, boolean aServoStatus) {
    int degreeNumber = 15;
    if (aServoStatus = false) {
        degreeNumber = 170;
    }
}

