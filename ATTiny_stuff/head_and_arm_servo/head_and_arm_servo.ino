#include <SoftwareServo.h> 

SoftwareServo servoArm;
SoftwareServo servoHead;

void setup() {
  servoArm.attach(0);
  servoArm.write(90);

  servoHead.attach(3);
  servoHead.write(90);
}

void loop() {
    SoftwareServo::refresh();
    servoArm.write(15);
    servoHead.write(170);
    SoftwareServo::refresh();    
    
    for (int i = 0; i < 30; i++) {
       delay(50);
       SoftwareServo::refresh();    
    }
    
    delay(1000);  
    SoftwareServo::refresh();
    servoArm.write(170);
    servoHead.write(15);
    SoftwareServo::refresh(); 
    
    for (int i = 0; i < 30; i++) {
        delay(50);
       SoftwareServo::refresh();    
    }
    
    delay(1700);
}

