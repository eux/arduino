const int buzzerPin = 9;

const double DO_C_4 = 30.0;
const double RE_D_4 = 27.0;

void play(double note, int length) {
  for (int i = 0; i < length; i++) {
    digitalWrite(buzzerPin, LOW);
    digitalWrite(buzzerPin, HIGH);
    delay(note);
  }
} 

void setup() {
  pinMode(buzzerPin, OUTPUT);
}

void loop() {
  play(DO_C_4, 25);
  
  delay(100);
  
  play(DO_C_4, 25);
  
  delay(100);
  
  play(RE_D_4, 50);
  
  play(DO_C_4, 25);
  
  delay(2000);
  
  
  
  
  /*
  for (int i = 0; i < 200; i++) {
    digitalWrite(buzzerPin, LOW);
    digitalWrite(buzzerPin, HIGH);
    delay(3);
    
  }
  */
  
  
  /*
  for (int i = 0; i < 100; i++) {
    digitalWrite(buzzerPin, HIGH);
    delay(i);
    digitalWrite(buzzerPin, LOW);
    delay(5);
  }
  */
  
  
  
}
