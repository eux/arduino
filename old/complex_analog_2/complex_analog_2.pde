//this program controls a blinking led with a frequency dependent on a resistor 
//with an on/off button

const int LED = 13;               
const int BUTTON = 2;   
int originalVal = 0;   // variable used to store the value
               // coming from the sensor
int buttonState = 0; 
int buttonVal = 0; 
int buttonOldVal = 0; 

int ledStatus;
int val;

void setup() {
  pinMode(LED, OUTPUT); // LED is as an OUTPUT
  // initialize the pushbutton pin as an input:
  pinMode(BUTTON, INPUT);  
  
  Serial.begin(9600);
  
  val = 0;
  ledStatus = LOW;
}

void loop() {
  
  if (ledStatus == LOW && val == 0) {
    Serial.println("entrato");
    originalVal = analogRead(0); // read the value from
    ledStatus = HIGH; 
    val = originalVal;
  } 
                       
  buttonVal = digitalRead(BUTTON); 
  if ((buttonVal == HIGH) && (buttonOldVal == LOW)){ 
    buttonState = 1 - buttonState;
    delay(10);
  } 
  
  buttonOldVal = buttonVal;
  
  Serial.println(buttonState);
  Serial.println(ledStatus);
  
  if (buttonState == HIGH) { 
    
    Serial.println(val);
    
    digitalWrite(LED, ledStatus); // turn the LED on
  
    delay(10); // stop the program for
                // some time
  
    val--;
    if(val == -1) {
      val = originalVal;
      buttonOldVal == LOW;
    }
  } else {
    digitalWrite(LED, LOW);
  }
}

