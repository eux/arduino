//this program controls a blinking led with a frequency dependent on a resistor 
//with an on/off button

const int LED = 13;               
const int BUTTON = 2;   
int val = 0;   // variable used to store the value
               // coming from the sensor
int buttonState = 0; 
int buttonVal = 0; 
int buttonOldVal = 0; 

void setup() {
  pinMode(LED, OUTPUT); // LED is as an OUTPUT
  // initialize the pushbutton pin as an input:
  pinMode(BUTTON, INPUT);  
  
  Serial.begin(9600);
}

void loop() {

  val = analogRead(0); // read the value from
                       // the sensor
  buttonVal = digitalRead(BUTTON); 
  if ((buttonVal == HIGH) && (buttonOldVal == LOW)){ 
    buttonState = 1 - buttonState;
    delay(10);
  } 
  
  buttonOldVal = buttonVal;
  
  Serial.println(buttonState);
  
  if (buttonState == HIGH) { 
    
    Serial.println(val);
    
    digitalWrite(LED, HIGH); // turn the LED on
  
    delay(val); // stop the program for
                // some time
  
    digitalWrite(LED, LOW); // turn the LED off
  
    delay(val); // stop the program for
                // some time
  } else {
    digitalWrite(LED, LOW);
  }
}

