/*
  Melody
 
 Plays a melody
 
 circuit:
 * 8-ohm speaker on digital pin 8
 
 created 21 Jan 2010
 modified 30 Aug 2011
 by Tom Igoe
 
 This example code is in the public domain.
 
 http://arduino.cc/en/Tutorial/Tone
 
 */
#include "pitches.h"

int potPin2 = 2; 
int potPin3 = 3; 
int val1 = 0;
int val2 = 0;
// notes in the melody:
int melody[] = {
NOTE_C4,
NOTE_DS4,
NOTE_E4,
NOTE_FS4,
NOTE_A4,
NOTE_B4,
NOTE_C5,
NOTE_DS5,
NOTE_E5,
NOTE_FS5,
NOTE_A5,
NOTE_B5
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4 };

void setup() {
  Serial.begin(9600);
}

void loop() {
  val1 = analogRead(potPin2); 
  val2 = analogRead(potPin3);
  Serial.print(val1);
  Serial.print(" - ");
  Serial.print(val2);
  Serial.print("\n");
  
  // iterate over the notes of the melody:
  int thisNote = map(val1, 0, 1023, 0, 11);
 
  //for (int thisNote = 0; thisNote < 8; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000/map(val2, 0, 1023, 2, 32);
    tone(8, melody[thisNote],noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 0.10;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(8);
  //}
  
 // delay(500);
}

