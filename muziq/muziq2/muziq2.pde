/*
  Melody
 
 Plays a melody
 
 circuit:
 * 8-ohm speaker on digital pin 8
 
 created 21 Jan 2010
 modified 30 Aug 2011
 by Tom Igoe
 
 This example code is in the public domain.
 
 http://arduino.cc/en/Tutorial/Tone
 
 */
#include "pitches.h"

int potPin2 = 2; 
//int potPin3 = 3; 
int val1 = 0;
int val2 = 0;
// notes in the melody:
int melody[] = {
NOTE_C4,
NOTE_DS4,
NOTE_E4,
NOTE_FS4,
NOTE_A4,
NOTE_B4,
NOTE_C5,
NOTE_DS5,
NOTE_E5,
NOTE_FS5,
NOTE_A5,
NOTE_B5
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4 };

void setup() {
  Serial.begin(9600);
  
  int val1 = 0;
}

void loop() {
  
  val1 = analogRead(potPin2); 
  
  val1 = map(val1, 0, 1024, 20, 500);
  
  Serial.println(val1);
  
    tone(8, NOTE_C4, 1000);
    delay(val1);
    tone(8, NOTE_DS4, 1000);
    delay(val1);
    tone(8, NOTE_E4, 1000);
    delay(val1);
    tone(8, NOTE_FS4, 1000);
    delay(val1);
    tone(8, NOTE_A4, 1000);
    delay(val1);

    delay(200);
    // stop the tone playing:
    noTone(8);
  //}
  
 // delay(500);
}

