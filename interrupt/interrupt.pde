/**
 * Interrupt Test - Alfieri Mauro
 *
 * Web: www.mauroalfieri.it
 * Tw:  @mauroalfieri
 */
 
int ledRed=8;
int ledGreen=7;
int ledYellow=12;
int interrCount=0;
volatile int yellowOn = false;
 
void setup()
{
  pinMode(ledRed, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(ledYellow, OUTPUT);    
 
  digitalWrite(ledRed, LOW);
  digitalWrite(ledGreen, LOW);
  digitalWrite(ledYellow, LOW);
 
  attachInterrupt(0, interruptGiallo, RISING);
}
 
void loop() {
  interrCount++;
 
  digitalWrite(ledRed, HIGH);
  digitalWrite(ledGreen, LOW);
  digitalWrite(ledYellow, LOW);
  delay(300);
  digitalWrite(ledRed, LOW);
  digitalWrite(ledGreen, HIGH);
  digitalWrite(ledYellow, LOW);
  delay(300);
  
  if (yellowOn) {
    digitalWrite(ledYellow, HIGH);
    digitalWrite(ledRed, LOW);
    digitalWrite(ledGreen, LOW);
    delay(300);
  }
}
 
void interruptGiallo()
{
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200) 
  {
    yellowOn = !yellowOn;
  }
  last_interrupt_time = interrupt_time;
}
