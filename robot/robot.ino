/* 
 Plays random notes from a pentatonic sequence with potentiometer control of note heights and tempo
 
 for a fritzing circuit representation see fritzing folder
 
 created 19 Jan 2013
 by Eugenio Pombi
 @euxpom
 
 This example code is in the public domain.
 */
#include "pitches.h"
#include "pentatonic.h"

int speakerOut = 13; 
int potentiometerPin1 = 1;  //Analog in 1 - speed 
int potentiometerPin2 = 2;  //Analog in 2 - height of melody's notes
int motorPin = 9;           //pin for the motor transistor control
int motorControl = 2;   

int melodySpeed = 0;
int melodyHeight = 0;  
int randomNumber = 0;  
int motorStatus = false;  

void setup() {
  Serial.begin(9600);
  pinMode(motorPin, OUTPUT); 
  pinMode(motorControl, INPUT); 
  
  int melodySpeed = 0;
  int melodyHeight = 0;
  int randomNumber = 0;
  
  analogWrite(motorPin, 0);
}

void loop() 
{
  motorStatus = digitalRead(motorControl);
  
  if (motorStatus == HIGH) {
    analogWrite(motorPin, 50);
  } else {
    analogWrite(motorPin, 0);
  }
    
  melodySpeed = analogRead(potentiometerPin1); 
  melodySpeed = map(melodySpeed, 0, 1024, 20, 500);
  
  melodyHeight = analogRead(potentiometerPin2); 
  melodyHeight = map(melodyHeight, 0, 1024, 0, 4);
  
  randomNumber = random(0, 4);
  

  Serial.print("height: ");
  Serial.print(melodyHeight);
  Serial.print("; ");
  Serial.print("speed: ");
  Serial.print(melodySpeed);
  Serial.print("\n");
  Serial.print("random: ");
  Serial.print(randomNumber);
  Serial.print("\n");
 
  
  tone(speakerOut, pentatonic[melodyHeight][randomNumber], 1000);
  delay(melodySpeed);
  
  // stop the tone playing:
  noTone(speakerOut);  
}


