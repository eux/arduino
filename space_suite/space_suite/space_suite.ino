int led1 = 12;
int led2 = 11;
int led3 = 10;
int led4 = 9;

int sensor = 0;

void setup() 
{                
  Serial.begin(115200);
  
  pinMode(led1, OUTPUT);  
  pinMode(led2, OUTPUT);  
  pinMode(led3, OUTPUT);  
  pinMode(led4, OUTPUT);  
  
  pinMode(sensor, INPUT);  
}

void loop() 
{
  int sensorRead = analogRead(sensor);
  
  int ledNumber = map(sensorRead, 0, 1024, 0, 5);
  
  Serial.print("sensorRead=");
  Serial.print(sensorRead);
  Serial.print("; ");
  Serial.print("ledNumber=");
  Serial.print(ledNumber);
  Serial.print("; \n");
  
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(led4, LOW);
  
  if (ledNumber >= 1) {
      digitalWrite(led1, HIGH);
  }
  
  if (ledNumber >= 2) {
      digitalWrite(led2, HIGH);
  }
  
  if (ledNumber >= 3) {
      digitalWrite(led3, HIGH);
  }
  
  if (ledNumber >= 4) {
      digitalWrite(led4, HIGH);
  }
  
  delay(300);
}
