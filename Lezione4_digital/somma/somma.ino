int a;                                  //Definisco le variabili che utilizzerò nel programma.
int b;                                  //int: numeri interi (1 ; 3 ; 10 ; 12635 ecc)
int somma;                              //float: numeri con la virgola (0,1 ; 3.14 ; 23,987 ; 435.1ecc)
                                        
void setup(){                           //Il setup si ripete ogni volta che premo il tasto reset

  Serial.begin(9600);                   //Definisco la velocità con cui Arduino invia i dati al pc(baud rate).
  a=2;                                  //Attribuisco un valore alla variabile a
  b=3;                                  //Attribuisco un valore alla variabile b

  somma=a+b;                            //Attribuisco il valore alla variabile "somma" come somma di "a" e "b"

  Serial.println("Addizione:");         //Scrivi "Addizione:" e poi vai a capo
  Serial.print(a);                      //Scrivi il valore della variabile "a"
  Serial.print("+");                    //Scrivi "+"
  Serial.print(b);                      //Scrivi il valore della variabile "b"
  Serial.print("=");                    //Scrivi "="
  Serial.println(somma);                //Scrivi il valore della variabile "somma" e poi vai a capo

}

void loop(){
}                                        //Non ci scrivo nulla, il programma fa una volta sola quello che c'è nel setup e basta. 

