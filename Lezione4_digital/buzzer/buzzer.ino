//Riadattamento del codice su http://learn.adafruit.com/adafruit-arduino-lesson-10-making-sounds/playing-a-scale

int buzzer = 9;
int numNote = 8;
int nota[] = {261, 294, 330, 349, 392, 440, 495, 528};
//            do    re   mi   fa  sol  la   si   do
 
void setup(){
  
  for (int i = 0; i < numNote; i++){
    tone(buzzer, nota[i]);
    delay(500);
  }
  noTone(buzzer);
}
 
void loop(){
  
}
