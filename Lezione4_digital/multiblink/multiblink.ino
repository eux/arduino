int led1=9;                                        //Definisco le variabili che utilizzerò nel programma.
int led2=10;
int led3=11;
int i;

void setup(){
  pinMode(led1, OUTPUT);                           //Il pin "led1" è in modalità OUTPUT
  pinMode(led2, OUTPUT);                           //Il pin "led2" è in modalità OUTPUT
  pinMode(led3, OUTPUT);                           //Il pin "led3" è in modalità OUTPUT
}

void loop(){                                       //Loop: ripeti le azioni seguenti all'infinito
  
  for(i=9;i<=11;i++){                              //Fai il ciclo per i che va da 9 fino a 11
     digitalWrite(i, HIGH);                        //Accendi il pin numero "i"
     delay(150);                                   //Mantienilo acceso per 150 ms
     digitalWrite(i, LOW);                         //Spegnilo
  }
  
}
