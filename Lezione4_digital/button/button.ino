int lucetta=9;
int pulsante=7;
int statoPuls;

void setup(){
  pinMode(lucetta, OUTPUT);
  pinMode(pulsante, INPUT);
  Serial.begin(9600);
}

void loop(){
  statoPuls=digitalRead(pulsante);
  
  if(statoPuls==LOW){
    Serial.println("Pulsante non premuto!");
    digitalWrite(lucetta, LOW);
  }
  else{
    Serial.println("Pulsante premuto!");
    digitalWrite(lucetta, HIGH);
  }

}
