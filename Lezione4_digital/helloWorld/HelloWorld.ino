//Con questo sketch Arduino spedisce al serial monitor un messaggio 
//ogni volta che si preme il tasto reset.
                                     
void setup(){                           //Il setup si ripete ogni volta che premo il tasto reset

  Serial.begin(9600);                   //Definisco la velocità con cui Arduino invia i dati al pc(baud rate).
  Serial.println("Hello world!");       //Stampa sul serial monitor "Hello world!"

}

void loop(){
                                        //Non ci scrivo nulla, il programma fa una volta sola quello che c'è nel setup e basta. 
}
