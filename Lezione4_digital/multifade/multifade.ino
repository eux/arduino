int led1=9;                                        //Definisco le variabili che utilizzerò nel programma.
int led2=10;
int led3=11;
int i;
int j;

void setup(){
  pinMode(led1, OUTPUT);                           //Il pin "led1" è in modalità OUTPUT
  pinMode(led2, OUTPUT);                           //Il pin "led2" è in modalità OUTPUT
  pinMode(led3, OUTPUT);                           //Il pin "led3" è in modalità OUTPUT
}

void loop(){                                       //Loop: ripeti le azioni seguenti all'infinito
  
  for(i=9;i<=11;i++){
    
     for(j=0;j<=255;j++){
       analogWrite(i, j);                          //analogWrite(pin, intensità)
       delay(10);
     }  
     
  }
  
  digitalWrite(led1, LOW);                         // Spegni tutto e aspetta 1 secondo prima di ricominciare
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  delay(1000);
  
}
