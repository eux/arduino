int puls1=5;
int puls2=6;
int puls3=7;
int buzzer=9;

void setup(){
  pinMode(puls1, INPUT);
  pinMode(puls2, INPUT);
  pinMode(puls3, INPUT);
  pinMode(buzzer, OUTPUT);
  
  Serial.begin(9600);

}

void loop(){
  
  if(digitalRead(puls1)){
    Serial.println("DO");
    tone(buzzer, 261);
  }
  if(digitalRead(puls2)){
    Serial.println("RE");
    tone(buzzer, 294);
  }
  if(digitalRead(puls3)){
    Serial.println("MI");
    tone(buzzer, 330);
  }
  
  delay(200);
  noTone(buzzer);  
  
}
