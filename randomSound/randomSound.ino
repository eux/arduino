const int speaker = 5;
int pause = 2000;

void setup() {
  // set the switch as an input:
  pinMode(speaker, OUTPUT);
  
}

void loop() {
  /*
  for (int i = 0; i < 5; i++) {
    for (int e = 0; e < 1000; e++) {
      tone(speaker, e);
      delayMicroseconds(pause - (e * 2));
    }
  }
  */
  
  for (int i = 0; i < 5; i++) {
    int toneValue = i * 1000;
    
    for (int e = 0; e < 1000; e++) {
      if (toneValue > 500) {
        tone(speaker, toneValue - e);
        delayMicroseconds(pause - (e * 2));  
      } else {
        tone(speaker, toneValue + e);
        delayMicroseconds(pause - (e * 2));
      }
    }
  }
  
  for (int e = 0; e < 100; e++) {
    tone(speaker, random(400, 600));
    delayMicroseconds(random(100, 500) * e);
  }
  
  
  
  
}

