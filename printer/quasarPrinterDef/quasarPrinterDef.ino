/*************************************************************************
  This is an Arduino library for the Adafruit Thermal Printer.
  Pick one up at --> http://www.adafruit.com/products/597
  These printers use TTL serial to communicate, 2 pins are required.

  Adafruit invests time and resources providing this open source code.
  Please support Adafruit and open-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution.
 *************************************************************************/

// If you're using Arduino 1.0 uncomment the next line:
#include "SoftwareSerial.h"
// If you're using Arduino 23 or earlier, uncomment the next line:
//#include "NewSoftSerial.h"

#include "Adafruit_Thermal.h"
#include "adalogo.h"
#include "adaqrcode.h"
#include <avr/pgmspace.h>

int printer_RX_Pin = 5;  // This is the green wire
int printer_TX_Pin = 6;  // This is the yellow wire

Adafruit_Thermal printer(printer_RX_Pin, printer_TX_Pin);

const int buttonPin = 2;
const int redLedPin =  13;
const int greenLedPin =  10;

boolean mustLoad = false;

void setup(){
  Serial.begin(9600);
  
  pinMode(buttonPin, INPUT);
  pinMode(redLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);
  
  digitalWrite(redLedPin, HIGH); 
}

void loop() {
    /*
    if (mustLoad) {
        digitalWrite(redLedPin, HIGH);
        digitalWrite(greenLedPin, LOW);
        
        delay(3000);
        digitalWrite(redLedPin, LOW);
        digitalWrite(greenLedPin, HIGH);
        mustLoad = false;
        
        Serial.println("Finita pausa");
    }
    */
    
    boolean reading = digitalRead(buttonPin);
    Serial.println(reading);
    
    if (reading) {
        Serial.println("Stampo");
        digitalWrite(greenLedPin, LOW);
        
        printFlyer();
        
        digitalWrite(redLedPin, HIGH);
        digitalWrite(greenLedPin, LOW);
        //mustLoad = true;
    }
}

void printFlyer() {
    Serial.println("START");
    
    pinMode(7, OUTPUT); digitalWrite(7, LOW); // To also work w/IoTP printer
  printer.begin();

  // The following function calls are in setup(), but do not need to be.
  // Use them anywhere!  They're just here so they're run only one time
  // and not printed over and over.
  // Some functions will feed a line when called to 'solidify' setting.
  // This is normal.


  printer.setSize('L'); 
  printer.boldOn();
  printer.justify('L');
  printer.feed(1);
  //printer.doubleHeightOn();
  printer.println("********************************");
  
  printer.println("QUASAR");
  printer.println("DESIGN");
  printer.println("UNIVERSITY");
  //printer.doubleHeightOff();
  printer.boldOff();
  printer.justify('L');
  printer.println("********************************");
  printer.feed(1);
  
  
  printer.setSize('S');
  printer.println("Progetta il tuo futuro,");
  printer.println("dai forma alla tua creativita'.");
  printer.println("Vieni a trovarci all'Istituto");
  printer.println("Quasar e scopri i nostri corsi");
  printer.println("e i nostri master.");
  printer.feed(1);
  
  
  printer.setSize('M');
  printer.justify('C');
  printer.boldOn();
  printer.println("Open Day:");
  printer.println("24 Settembre / 14 Ottobre");
  printer.boldOff();
  
  printer.setSize('S');
  printer.justify('R');
  printer.println("Info e prenotazioni:"); 
  printer.println("www.istitutoquasar.com");
  printer.println("Tel. 06 855 70 78");
    printer.feed(6);
  
  printer.sleep();      // Tell printer to sleep
  printer.wake();       // MUST call wake() before printing again, even if reset
  printer.setDefault(); // Restore printer to defaults
  
 Serial.println("END");
}
