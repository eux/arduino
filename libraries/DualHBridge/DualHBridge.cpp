/*
  DualHBridge.h - Library for controlling two motors attached to a dual H-Bridge.
  Created by Eugenio Pombi, April 25, 2013.
  Released into the public domain.
*/

#include "Arduino.h"
#include "DualHBridge.h"
    
DualHBridge::DualHBridge()
{ 

}
    

DualHBridge::DualHBridge(int leftEnable, int leftPin1, int leftPin2, int rightEnable, int rightPin1, int rightPin2)
{ 
  pinMode(leftEnable, OUTPUT);
  pinMode(leftPin1, OUTPUT);
  pinMode(leftPin2, OUTPUT);
  
  pinMode(rightEnable, OUTPUT);
  pinMode(rightPin1, OUTPUT);
  pinMode(rightPin2, OUTPUT);
  
  _leftEnable = leftEnable;
  _leftPin1 = leftPin1;
  _leftPin2 = leftPin2; 
  
  _rightEnable = rightEnable;
  _rightPin1 = rightPin1;
  _rightPin2 = rightPin2;
}

void DualHBridge::attachLeftEnablePin(int pin)
{
  _leftEnable = pin;
  pinMode(pin, OUTPUT);  
}

void DualHBridge::attachLeftPin1(int pin)
{
  _leftPin1 = pin;
  pinMode(pin, OUTPUT);  
}

void DualHBridge::attachLeftPin2(int pin)
{
  _leftPin2 = pin;
  pinMode(pin, OUTPUT);  
}

void DualHBridge::attachRightEnablePin(int pin)
{
  _rightEnable = pin;
  pinMode(pin, OUTPUT);  
}

void DualHBridge::attachRightPin1(int pin)
{
  _rightPin1 = pin;
  pinMode(pin, OUTPUT);  
}

void DualHBridge::attachRightPin2(int pin)
{
  _rightPin2 = pin;
  pinMode(pin, OUTPUT);  
}

void DualHBridge::enableMotors()
{
  digitalWrite(_leftEnable, HIGH);   
  digitalWrite(_rightEnable, HIGH);  
}

void DualHBridge::moveForward()
{
  digitalWrite(_leftPin1, HIGH);   
  digitalWrite(_leftPin2, LOW);  
    
  digitalWrite(_rightPin1, HIGH);   
  digitalWrite(_rightPin2, LOW);
}

void DualHBridge::moveBalancedForward(int left, int right)
{
  analogWrite(_leftPin1, left);   
  analogWrite(_leftPin2, 0);  
    
  analogWrite(_rightPin1, right);   
  analogWrite(_rightPin2, 0);
}

void DualHBridge::moveBackward()
{
  digitalWrite(_leftPin1, LOW);   
  digitalWrite(_leftPin2, HIGH);  
    
  digitalWrite(_rightPin1, LOW);   
  digitalWrite(_rightPin2, HIGH);
}

void DualHBridge::halt()
{
  digitalWrite(_leftPin1, LOW);   
  digitalWrite(_leftPin2, LOW);  
    
  digitalWrite(_rightPin1, LOW);   
  digitalWrite(_rightPin2, LOW);
}

void DualHBridge::pivotRight()
{
  digitalWrite(_leftPin1, LOW);   
  digitalWrite(_leftPin2, HIGH);  
    
  digitalWrite(_rightPin1, HIGH);   
  digitalWrite(_rightPin2, LOW);
}

void DualHBridge::pivotLeft()
{
  digitalWrite(_leftPin1, HIGH);   
  digitalWrite(_leftPin2, LOW);  
    
  digitalWrite(_rightPin1, LOW);   
  digitalWrite(_rightPin2, HIGH);
}

void DualHBridge::moveRight()
{
  digitalWrite(_leftPin1, LOW);   
  digitalWrite(_leftPin2, LOW);  
    
  digitalWrite(_rightPin1, HIGH);   
  digitalWrite(_rightPin2, LOW);
}

void DualHBridge::moveLeft()
{
  digitalWrite(_leftPin1, HIGH);   
  digitalWrite(_leftPin2, LOW);  
    
  digitalWrite(_rightPin1, LOW);   
  digitalWrite(_rightPin2, LOW);
}

