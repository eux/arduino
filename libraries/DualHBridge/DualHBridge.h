/*
  DualHBridge.h - Library for controlling two motors attached to a dual H-Bridge.
  Created by Eugenio Pombi, April 25, 2013.
  Released into the public domain.
*/
#ifndef DualHBridge_h
#define DualHBridge_h

#include "Arduino.h"

class DualHBridge
{
  public:
    DualHBridge();
    DualHBridge(int leftEnable, int leftPin1, int leftPin2, int rightEnable, int rightPin1, int rightPin2);
    void attachLeftEnablePin(int pin);
    void attachLeftPin1(int pin);
    void attachLeftPin2(int pin);
    
    void attachRightEnablePin(int pin);
    void attachRightPin1(int pin);
    void attachRightPin2(int pin);
    
    void enableMotors();
    void moveForward();
    void moveBalancedForward(int left, int right);
    void moveBackward();
    void halt();
    void pivotLeft();
    void pivotRight();
    void moveLeft();
    void moveRight();
  private:
    int _leftEnable;
    int _leftPin1;
    int _leftPin2;
    int _rightEnable;
    int _rightPin1;
    int _rightPin2;
};

#endif
