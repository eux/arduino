ARDUINO STARTER KIT con ARDUINO UNO

DESCRIZIONE

Starter kit con Arduino UNO. Contiene tutto il necessario per utilizzare questa piattaforma hardware open-source ormai diffusissima in tutto il mondo. La confezione comprende: Arduino UNO, cavo USB, mini Breadboard 170 contatti, 10 cavetti da 10 cm intestati ai capi con connettore maschio a 1 polo, 2 motori elettrici 3 Vdc / 300 mA, ricevitore IR integrato, fotoresistenza, termistore, LED RGB, 10 LED rossi - 10 gialli e 10 verdi da 5 mm, 5 minipulsanti da C.S., 1 pulsante da pannello, CLIPS per batteria 9 Volt, 10 transistor NPN BC547B, 2 potenziometri Slider, relè miniatura 12 volt 1 scambio, display LCD retroilluminato 2 righe e 16 caratteri alfanumerici, integrato L298N (driver per motori DC e stepper), 10 resistenze da 330 ohm 1/4 di watt e 10 resistenze da 4,7 kohm 1/4 di watt.


CONTENUTO DELLA CONFEZIONE

Scheda ARDUINO UNO con ATmega328 - completamente assemblata e testata.
Cavo USB 2.0 maschio (A) / maschio (B). Lunghezza: 2 metri - per collegamento al PC .
Display LCD 2 righe e 16 caratteri alfanumerici con retroilluminazione. Utilizza il controller HD44780 (datasheet). Alimentazione: 5 Vdc. Dimensioni: 84 x 44 x 8,40 mm.
Mini Breadboard 170 contatti con base autoadesiva. Dimensioni 4,5 x 3,5 cm.
Confezione contenente 10 cavetti da 10 cm intestati ai capi con connettore maschio a 1 polo. Ideale per essere impiegato con le piastre sperimentali.
Sensibilissimo sensore IR con preamplificatore, demodulatore e filtro tarato sulla frequenza di 38 kHz - 600 µs con amplificatore/squadratore incorporato. Tre soli terminali, alimentazione a 5 V.
2 Pz. Motore elettrico miniaturizzato 3 Vdc / 300 mA per impieghi generici.
Driver doppio full bridge per motori DC e motori passo passo. Tensione di alimentazione da 4,5 a 7 V, tensione di alimentazione motore da 7 a 46 Vdc, corrente di picco in uscita per canale: 2 A, contenitore Multiwatt15H.
Fotoresistenza: è un componente elettronico la cui resistenza è inversamente proporzionale alla quantità di luce che lo colpisce. Si comporta come un tradizionale resistore, ma il suo valore in Ohm diminuisce mano a mano che aumenta l'intensità della luce che la colpisce.
Termistore: è un sensore utilizzato per convertire una temperatura in un valore rappresentativo di corrente elettrica, facilmente misurabile ad esempio con un galvanometro, soprattutto in applicazioni industriali e nel settore dell' automazione.
LED RGB 5 mm (rosso, verde e blu) ideale per creare qualsiasi colore.
10 LED rossi, 10 gialli e 10 verdi da 5 mm
Relè sigillato con terminali da circuito stampato. È caratterizzato da dimensioni estremamente compatte (15,2 x 10,3 x 11,4 mm), da una elevata sensibilità ed affidabilità. Alimentazione bobina: 12 Vdc / 13 mA, portata contatti: 1A/120 Vac.
5 Pz - Minipulsante per montaggio diretto su PCB. Dimensioni: 6x6 mm.
CLIPS 9 VOLT.
10 Pz - Transistor NPN BC547B.
2 Pz - Potenziometro Slider.
10 Pz - Resistenze da 330 ohm 1/4 di watt 5% - strato di carbone.
10 Pz - Resistenze da 4,7 kohm 1/4 di watt 5% - strato di carbone.



CARATTERISTICHE TECNICHE:

    Microcontrollore: ATmega328
    Tensione di funzionamento: 5 V
    Tensione di alimentazione (raccomandata): da 7 a 12 V
    Tensione di alimentazione (limiti): 6-20V
    Ingressi/uscite Digitali: 14 (di cui 6 possono essere utilizzate come uscite PWM)
    Ingressi analogici: 6
    Corrente Dc per pin I/O: 40 mA
    Corrente DC per pin 3,3 V: 50 mA
    Memoria Flash: 32 kB (di cui 0,5 kB utilizzati dal bootloader)
    SRAM: 2 kB
    EEPROM: 1 kB
    Velocità di Clock : 16 MHz


