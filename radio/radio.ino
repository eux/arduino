/**
 * A Mirf example to test the latency between two Ardunio.
 *
 * Pins:
 * Hardware SPI:
 * MISO -> 12
 * MOSI -> 11
 * SCK -> 13
 *
 * Configurable:
 * CE -> 8
 * CSN -> 7
 *
 * Note: To see best case latency comment out all Serial.println
 * statements not displaying the result and load 
 * 'ping_server_interupt' on the server.
 */

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

int pinSend = 7;
int pinReceive = 8;

void setup(){
  Serial.begin(115200);
  /*
   * Setup pins / SPI.
   */
   
  /* To change CE / CSN Pins:
   * 
   * Mirf.csnPin = 9;
   * Mirf.cePin = 7;
   */
  
  Mirf.cePin = 9;
  Mirf.csnPin = 10;
  
  Mirf.spi = &MirfHardwareSpi;
  Mirf.init();
  
  /*
   * Configure reciving address.
   */
   
  Mirf.setRADDR((byte *)"clie1");
  
  /*
   * Set the payload length to sizeof(unsigned long) the
   * return type of millis().
   *
   * NB: payload on client and server must be the same.
   */
   
  Mirf.payload = sizeof(unsigned long);
  
  /*
   * Write channel and payload config then power up reciver.
   */
   
  /*
   * To change channel:
   * 
   * Mirf.channel = 10;
   *
   * NB: Make sure channel is legal in your area.
   */

  Mirf.channel = 10;
  Mirf.config();
  
  pinMode(pinSend, OUTPUT);
  pinMode(pinReceive, OUTPUT);
  Serial.println("Beginning ... "); 
  
  digitalWrite(pinSend, LOW);
  digitalWrite(pinReceive, LOW);
}

void loop(){
  digitalWrite(pinSend, HIGH);
  unsigned long payload = 123456;
  unsigned long time = millis();
  
  Mirf.setTADDR((byte *)"serv1");
  
  Mirf.send((byte *)&payload);
  
  while(Mirf.isSending()){
  }
  Serial.println("Finished sending");
  delay(10);
  digitalWrite(pinSend, LOW);
  
  digitalWrite(pinReceive, HIGH);
  while(!Mirf.dataReady()){
    Serial.println("Waiting");
    if ( ( millis() - time ) > 2000 ) {
    Serial.println("Timeout on response from server!");
      digitalWrite(pinReceive, LOW);
      return;
    }
  }
  digitalWrite(pinReceive, LOW);
  
  unsigned long received = 0;
  Mirf.getData((byte *) &received);
  
  
  Serial.print("Received: ");
  Serial.print(received);
  Serial.print(": ");
  Serial.println((millis() - time));
  
  if(received == payload) {
    for (int c = 0; c < 5; c++) {
      digitalWrite(pinReceive, HIGH);
      digitalWrite(pinSend, HIGH);
      delay(200);
      digitalWrite(pinReceive, LOW);
      digitalWrite(pinSend, LOW);
      delay(200);
    }
  }
  
  
} 
  
  
  


