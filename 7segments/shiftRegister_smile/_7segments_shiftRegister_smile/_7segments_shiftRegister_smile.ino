//inspired by http://arduino.cc/en/Tutorial/ShiftOut
//Pin connected to ST_CP (12) of 74HC595 named (RCK on M74HC595B1)
int latchPin = 5;
//Pin connected to SH_CP (11) of 74HC595 named (SCK on M74HC595B1)
int clockPin = 13;
////Pin connected to DS (14) of 74HC595 named (SER on M74HC595B1)
int dataPin = 11;

//pins of the ground for the four different digits
int digit1 = 12;
int digit2 = 9;
int digit3 = 8;
int digit4 = 6;

int segmentDisplays[4] = {
  digit1,
  digit2,
  digit3,
  digit4,
};

int smile[4] = {
  B10110010,
  B10000010,
  B10000010,
  B10001110
};

void setup() 
{                
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  
  for (int digit = 0; digit < 4; digit++) {
    pinMode(segmentDisplays[digit], OUTPUT);
    digitalWrite(segmentDisplays[digit], HIGH);
  }
  
  Serial.begin(9600);
}


void loop() 
{  
  //Serial.println("---------------------");
  for (int digit = 0; digit < 4; digit++) {
    /*
    Serial.println("+++++++++++++++++++++");
    Serial.print("digit: ");
    Serial.print(digit);
    Serial.print("\n");
    Serial.print("segment: ");
    Serial.print(segmentDisplays[digit]);
    Serial.print("\n");
    Serial.print("smile: ");
    Serial.print(smile[digit]);
    Serial.print("\n");
    Serial.println("+++++++++++++++++++++");
    */
    digitalWrite(segmentDisplays[digit], LOW);
    shiftRegisterWrite(smile[digit]);
    
    delay(5);
    digitalWrite(segmentDisplays[digit], HIGH);    
  }
  //Serial.println("---------------------");
}

void shiftRegisterWrite(int value) 
{
  //prepare it to receive data
  digitalWrite(latchPin, LOW);
  //write data  
  shiftOut(dataPin, clockPin, MSBFIRST, value);
  //send data to pins
  digitalWrite(latchPin, HIGH);
}
