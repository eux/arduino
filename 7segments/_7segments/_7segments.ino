
int ch = 11;
int rh = 7;
int lh = 10;

int rl = 4;
int cl = 2;
int ll = 1;

int cm = 5;

int leftDigit = 12;
int rightDigit = 9;

int controlled;

void setup() {                
  pinMode(ch, OUTPUT);
  pinMode(rh, OUTPUT);  
  pinMode(lh, OUTPUT);  
  
  pinMode(rl, OUTPUT);  
  pinMode(cl, OUTPUT);  
  pinMode(ll, OUTPUT);  
  
  pinMode(cm, OUTPUT);

  pinMode(leftDigit, OUTPUT);
  pinMode(rightDigit, OUTPUT);  
  
  controlled = leftDigit;
}


void loop() { 
 
  digitalWrite(controlled, HIGH);
  
  digitalWrite(ch, HIGH);
  delay(200);
  digitalWrite(ch, LOW);
  digitalWrite(rh, HIGH);
  delay(200);
  digitalWrite(rh, LOW);
  digitalWrite(rl, HIGH);
  delay(200);
  digitalWrite(rl, LOW);
  digitalWrite(cl, HIGH);
  delay(200);
  digitalWrite(cl, LOW);
  digitalWrite(ll, HIGH);
  delay(200);
  digitalWrite(ll, LOW);
  digitalWrite(lh, HIGH);
  delay(200);
  digitalWrite(lh, LOW);
  
  digitalWrite(controlled, LOW);
  
  if (controlled == leftDigit) {
    controlled = rightDigit;
  } else {
    controlled = leftDigit; 
  }
}
