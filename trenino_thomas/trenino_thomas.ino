
#include "pitches.h"

//int speakerOut = 9;  //arduino
int speakerOut = 0;  //atTiny

void setup() {
  //Serial.begin(9600);
  
  pinMode(speakerOut, OUTPUT);  
}

void loop() {
  
  play(NOTE_F5, 300);
  delay(100);
  play(NOTE_G5, 300);
  delay(100);
  play(NOTE_A5, 300);
  delay(100);
  play(NOTE_AS5, 400);
  delay(200);
  play(NOTE_C6, 300);
  delay(100);
  play(NOTE_D6, 400);
  delay(200);
  play(NOTE_GS5, 800);
  delay(800);
  
  play(NOTE_A5, 200);
  delay(200);
  play(NOTE_G5, 200);
  delay(200);
  play(NOTE_A5, 200);
  delay(200);
  play(NOTE_G5, 200);
  delay(400);
  play(NOTE_F5, 200);
  delay(200);
  play(NOTE_G5, 400);
  delay(400);
  play(NOTE_A5, 200);
  delay(200);
  play(NOTE_G5, 200);
  delay(200);
  play(NOTE_A5, 300);
  delay(200);
  play(NOTE_B5, 600);
  delay(200);
  
  delay(1000);
}

void play(double note, float length) {
  float noteMicroFrequency = 1000000 / note;
  float nLoops = ((float) length * 1000) / noteMicroFrequency;
  int loops = round(nLoops);
  
  /*
  Serial.print("Length: ");
  Serial.print(length);
  Serial.print("\n");
  Serial.print("noteMicroFrequency: ");
  Serial.print(noteMicroFrequency);
  Serial.print("\n");
  Serial.print("nLoops: ");
  Serial.print(nLoops);
  Serial.print("\n");
  Serial.print("Loops: ");
  Serial.print(loops);
  Serial.print("\n");
  Serial.print("-----------------\n");
  */
  
  for (int i = 0; i < loops; i++) {  
    digitalWrite(speakerOut, LOW);
    digitalWrite(speakerOut, HIGH);
    
    delayMicroseconds(noteMicroFrequency);
  }
}

