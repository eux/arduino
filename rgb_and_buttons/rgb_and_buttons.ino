int buttonRedPin = 13;
int buttonGreenPin = 12;
int buttonBluePin = 8;
int buttonResetPin = 2;

int buttonRedStatus = LOW;
int buttonGreenStatus = LOW;
int buttonBlueStatus = LOW;
int buttonResetStatus = LOW;

int red = 0;
int green = 0;
int blue = 0;

int redPin = 11;
int greenPin = 9;
int bluePin = 10;

int increment = 50;

void setup () {
    pinMode(buttonRedPin, INPUT);
    pinMode(buttonGreenPin, INPUT);
    pinMode(buttonBluePin, INPUT);
    pinMode(buttonResetPin, INPUT);
    
    pinMode(redPin, OUTPUT);
    pinMode(greenPin, OUTPUT);
    pinMode(bluePin, OUTPUT);
    
    Serial.begin(9600);
}

void loop () {
    buttonRedStatus = digitalRead(buttonRedPin);
    buttonGreenStatus = digitalRead(buttonGreenPin);
    buttonBlueStatus = digitalRead(buttonBluePin);
    buttonResetStatus = digitalRead(buttonResetPin);
    
    Serial.println(buttonRedStatus);
    Serial.println(buttonGreenStatus);
    Serial.println(buttonBlueStatus);
    Serial.println(red);
    Serial.println(green);
    Serial.println(blue);
    Serial.println("======================");
    
    if (buttonRedStatus == HIGH ) {
        red = red + increment;
        delay(200);
    }
    if (buttonBlueStatus == HIGH ) {
        blue = blue + increment;
        delay(200);
    }
    if (buttonGreenStatus == HIGH ) {
        green = green + increment;
        delay(200);
    }
    if (buttonResetStatus == HIGH ) {
        green = 0;
        red = 0;
        blue = 0;
        delay(200);
    }
    
    if (red > 250) {
        red = 250;
    }
    if (green > 250) {
        green = 250;
    }
    if (blue > 250) {
        blue = 250;
    }
    
   analogWrite(redPin, red);
   analogWrite(bluePin, blue);
   analogWrite(greenPin, green);
}

void resetColor (int color) {
    if (color > 250) {
        color = 0;
    }
}


